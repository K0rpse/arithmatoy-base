# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return n*"S"+"0"

def S(n: str) -> str:
    return "S"+n


def addition(a: str, b: str) -> str:
    return (a.count("S") + b.count("S")) * "S" + "0"
    # if a == "0":
    #     return b
    # elif(a.startswith('S')):
    #     return S(addition(a[1:], b))

def multiplication(a: str, b: str) -> str:
    return (a.count("S") * b.count("S")) * "S" + "0"


def facto_ite(n: int) -> int:
    resultat = 1
    for i in range(1, n):
        resultat += resultat * i
    return resultat


def facto_rec(n: int) -> int:
    if(n==0):
        return 1
    return n * facto_rec(n-1)


def fibo_rec(n: int) -> int:
    if(n==0):
        return 0
    return n+fibo_rec(n-1)


def fibo_ite(n: int) -> int:
    a = 0
    b = 1
    current = 0
    for i in range(n):
        current = a+b
        tmp = a
        a = current
        b = tmp
    return current

def golden_phi(n: int) -> int:
    x = 1
    y = 1
    for i in range(n):
        temp = y
        y = x + y
        x = temp

    return y / x

def sqrt5(n: int) -> int:
    guess = 2.0
    tolerance = 0.0001
    
    #Babylonian method
    while abs(guess*guess - n) > tolerance:
        guess = (guess + n / guess) / 2
    
    return guess



def pow(a: float, n: int) -> float:
    if n == 0:
        return 1

    elif n % 2 == 0:
        return pow(a*a, n/2)
    else:
        return a * pow(a, n-1)
