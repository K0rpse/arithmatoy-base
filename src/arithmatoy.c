#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *supprimerZerosInutiles(char *chaine) {
  int longueur = strlen(chaine);
  int i = 0;

  // Trouver l'index du premier chiffre non nul
  while (chaine[i] == '0' && i < longueur - 1) {
    i++;
  }

  // Allouer de la mémoire pour la nouvelle chaîne de caractères
  char *nouvelleChaine = (char *)malloc(sizeof(char) * (longueur - i + 1));

  // Copier les caractères restants dans la nouvelle chaîne de caractères
  int j = 0;
  while (i < longueur) {
    nouvelleChaine[j] = chaine[i];
    i++;
    j++;
  }
  nouvelleChaine[j] = '\0';

  return nouvelleChaine;
}

char *arithmatoy_add(unsigned int base, const char *a, const char *b) {

  int len_a = strlen(a);
  int len_b = strlen(b);
  int max_length = (len_a > len_b) ? len_a : len_b;
  int *digits_a = malloc(max_length * sizeof(int));
  int *digits_b = malloc(max_length * sizeof(int));
  int *result = malloc((max_length + 1) * sizeof(int));

  // Convertit les nombres en listes de chiffres en inversant l'ordre
  for (int i = 0; i < max_length; i++) {
    digits_a[i] = (i < len_a)
        ? (isdigit(a[len_a - i - 1]) ? a[len_a - i - 1] - '0'
                                     : a[len_a - i - 1] - 'a' + 10)
        : 0;
    digits_b[i] = (i < len_b)
        ? (isdigit(b[len_b - i - 1]) ? b[len_b - i - 1] - '0'
                                     : b[len_b - i - 1] - 'a' + 10)
        : 0;
  }

  int carry = 0; // Initialise la retenue à 0

  // Effectue l'addition en ajoutant les chiffres correspondants et la retenue
  for (int i = 0; i < max_length; i++) {
    int digit_sum = carry;
    if (i < len_a) {
      digit_sum += digits_a[i];
    }
    if (i < len_b) {
      digit_sum += digits_b[i];
    }
    result[i] =
        digit_sum % base; // Stocke le chiffre de droite dans le résultat
    carry = digit_sum / base; // Calcule la nouvelle retenue
  }

  // Stocke la dernière retenue dans le résultat
  if (carry > 0) {
    result[max_length] = carry;
    max_length++;
  }

  // Convertit le résultat en une chaîne de caractères en utilisant les symboles
  // spécifiés dans la base
  char *output = malloc((max_length + 1) * sizeof(char));
  int index = 0;
  for (int i = max_length - 1; i >= 0; i--) {
    if (result[i] < 10) {
      output[index++] = result[i] + '0';
    } else {
      output[index++] = result[i] - 10 + 'a';
    }
  }
  output[index] = '\0';

  // Libère la mémoire allouée pour les tableaux de chiffres
  free(digits_a);
  free(digits_b);
  free(result);

  // Retourne la chaîne de caractères résultante
  return drop_leading_zeros(output);

  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

char *arithmatoy_sub(unsigned int base, const char *a, const char *b) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }
  a = drop_leading_zeros(a);
  b = drop_leading_zeros(b);
  // Vérifie si a est inférieur à b
  int a_length = 0;
  int b_length = 0;
  for (; a[a_length]; a_length++)
    ;
  for (; b[b_length]; b_length++)
    ;
  if (a_length < b_length) {
    return NULL;
  } else if (a_length == b_length && strcmp(a, b) < 0) {
    return NULL;
  }

  // Convertit a et b en listes de chiffres dans la base donnée
  int max_length = a_length > b_length ? a_length : b_length;
  int *digits_a = (int *)malloc(max_length * sizeof(int));
  int *digits_b = (int *)malloc(max_length * sizeof(int));
  int *result = (int *)malloc(max_length * sizeof(int));
  memset(digits_a, 0, max_length * sizeof(int));
  memset(digits_b, 0, max_length * sizeof(int));
  memset(result, 0, max_length * sizeof(int));
  for (int i = 0; i < a_length; i++) {
    digits_a[i] = get_digit_value(a[a_length - i - 1]);
  }
  for (int i = 0; i < b_length; i++) {
    digits_b[i] = get_digit_value(b[b_length - i - 1]);
  }

  // Effectue la soustraction en colonnes
  int borrow = 0;
  int index = 0;
  for (int i = 0; i < max_length; i++) {
    int diff = digits_a[i] - borrow;
    if (i < b_length) {
      diff -= digits_b[i];
    }
    if (diff < 0) {
      diff += base;
      borrow = 1;
    } else {
      borrow = 0;
    }
    result[index++] = diff;
  }

  // Convertit le résultat en une chaîne de caractères
  char *output = (char *)malloc((max_length + 1) * sizeof(char));
  int leading_zeros = 0;
  while (leading_zeros < max_length &&
         result[max_length - leading_zeros - 1] == 0) {
    leading_zeros++;
  }
  if (leading_zeros == max_length) {
    output[0] = '0';
    output[1] = '\0';
    return output;
  }
  int index_output = 0;
  for (int i = max_length - leading_zeros - 1; i >= 0; i--) {
    if (result[i] < 10) {
      output[index_output++] = result[i] + '0';
    } else {
      output[index_output++] = result[i] - 10 + 'a';
    }
  }
  output[index_output] = '\0';
  return drop_leading_zeros(output);
}

// Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
// You should allocate a new char* large enough to store the result as a
// string Implement the algorithm Return the result

char *arithmatoy_mul(unsigned int base, const char *a, const char *b) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Convertit a et b en listes de chiffres dans la base donnée
  int a_length = strlen(a);
  int b_length = strlen(b);
  int *digits_a = (int *)malloc(a_length * sizeof(int));
  int *digits_b = (int *)malloc(b_length * sizeof(int));
  memset(digits_a, 0, a_length * sizeof(int));
  memset(digits_b, 0, b_length * sizeof(int));
  for (int i = 0; i < a_length; i++) {
    digits_a[i] = get_digit_value(a[a_length - i - 1]);
  }
  for (int i = 0; i < b_length; i++) {
    digits_b[i] = get_digit_value(b[b_length - i - 1]);
  }

  // Effectue la multiplication en colonnes
  int result_length = a_length + b_length;
  int *result = (int *)malloc(result_length * sizeof(int));
  memset(result, 0, result_length * sizeof(int));
  for (int i = 0; i < b_length; i++) {
    int carry = 0;
    for (int j = 0; j < a_length; j++) {
      int product = digits_a[j] * digits_b[i] + carry + result[i + j];
      carry = product / base;
      result[i + j] = product % base;
    }
    if (carry != 0) {
      result[i + a_length] += carry;
    }
  }

  // Convertit le résultat en une chaîne de caractères
  char *output = (char *)malloc((result_length + 1) * sizeof(char));
  int leading_zeros = 0;
  while (leading_zeros < result_length &&
         result[result_length - leading_zeros - 1] == 0) {
    leading_zeros++;
  }
  if (leading_zeros == result_length) {
    output[0] = '0';
    output[1] = '\0';
    return output;
  }
  int index_output = 0;
  for (int i = result_length - leading_zeros - 1; i >= 0; i--) {
    if (result[i] < 10) {
      output[index_output++] = result[i] + '0';
    } else {
      output[index_output++] = result[i] - 10 + 'a';
    }
  }
  output[index_output] = '\0';

  // Libère la mémoire allouée pour les listes de chiffres et le résultat
  free(digits_a);
  free(digits_b);
  free(result);

  return output;

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
